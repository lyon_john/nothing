function display(arr) {
	var result = [];
	var new_arr = [];
	var finalResult;

	var type_name = {};
	var user_name = {};
	var product_name = {};

	var count = 0;
	var best_prod, worst_prod;
	var best_type, worst_type;
	var best_u, worst_u;

	//block 1.0*******************
	arr.forEach( function(element) {
		// transforming every element of the array into an array;
		new_arr = element.split(', ');

		// pushing every array into a new array;
		result.push(new_arr); 

	});

	//block 2.0*******************
	for(var i = 0; i < result.length; i++) {

		// counts how many times this kind of product was bought
		if (type_name[result[i][4]] != null) {
			type_name[result[i][4]] += 1;
		} else {
			type_name[result[i][4]] = 1;
		}

		// counts how many purcheses every user made
		if (user_name[result[i][1]] != null) {
			user_name[result[i][1]] += 1;
		} else {
			user_name[result[i][1]] = 1;
		}

		// counts how many times the product was bought
		if (product_name[result[i][3]] != null) {
			product_name[result[i][3]] += 1;
		} else {
			product_name[result[i][3]] = 1;
		}

	}

	// block 3.1.1*******************
	// counts which user made the most purcheses
	for (var u_key in user_name) {

		if(user_name[u_key] > count) {
			count = user_name[u_key];
			best_u = u_key + ': ' + user_name[u_key];

		} else if (user_name[u_key] == count) {
			best_u += ', \n' + u_key + ': ' + user_name[u_key];
			// checks if there are more then one leaders
		}

	}

	
	// block 3.1.2*********
	// counts which user made least purchases
	for (u_key in user_name) {

		if (user_name[u_key] < count) {
			count = user_name[u_key];
			worst_u = u_key + ': ' + user_name[u_key];

		} else if (user_name[u_key] == count) {
			worst_u += ', \n' + u_key + ': ' + user_name[u_key];
			// checks if there are more then one draw backs
		}
		
	}

	// reseting count
	count = 0;
	// storing in a nice looking string
	var user_sort = 'The best buyer(s) is/are: \n"' + best_u + 
		'"; \n\nThe worst buyer(s) is/are : \n"' + worst_u + '";';

	// block 3.2.1*******************
	// counts which product was a bestseller
	for (var p_key in product_name) {

		if(product_name[p_key] > count) {
			count = product_name[p_key];
			best_prod = p_key + ': ' + product_name[p_key];

		} else if (product_name[p_key] == count) {
			best_prod += ', \n' + p_key + ': ' + product_name[p_key];
			// checks if there are more than one leaders
		}

	}

	// block 3.2.2*********
	// counts which product was sold least
	for (p_key in product_name) {
		
		if (product_name[p_key] < count) {
			count = product_name[p_key];
			worst_prod = p_key + ': ' + product_name[p_key]; 

		} else if (product_name[p_key] == count) {
			worst_prod += ', \n' + p_key + ': ' + product_name[p_key];
			// checks if there are more than one draw backs
		}

	}

	// reseting count
	count = 0;
	// storing in a nice looking string
	var product_sort = 'The bestseller product(s) is/are: \n"' + best_prod + 
		'"; \n\nThe worst selling product(s) is/are : \n"' + worst_prod + '";';

	// block 3.3.1*******************
	// counts which type of product was sold least
	for (var t_key in type_name) {

		if (type_name[t_key] > count) {
			count = type_name[t_key];
			best_type = t_key + ': ' + type_name[t_key];

		} else if (type_name[t_key] == count) {
			best_type += ', \n' + t_key + ': ' + type_name[t_key];
		}// checks if there are more then one leaders

	}

	// block 3.3.2*********
	// counts which type of product was sold least
	for (t_key in type_name) {

		if (type_name[t_key] < count) {
			count = type_name[t_key];
			worst_type = t_key + ': ' + type_name[t_key];

		} else if (type_name[t_key] == count) {
			worst_type += ', \n' + t_key + ': ' + type_name[t_key];
		}// checks if there are more then one draw backs

	}
	
	// reseting count
	count = 0;
	// storing in a nice looking string
	var type_sort = 'The bestseller type of product(s) is/are: \n"' + 
		best_type +	
		'"; \n\nThe worst selling type of product(s) is/are : \n"' + 
		worst_type + '";';


	finalResult = user_sort + '\n\n\n' + product_sort + '\n\n\n' +
		type_sort;
	return finalResult;
}

console.log(display(data));
// The best buyer(s) is/are: 
// "Gene Jefferis: 72"; 

// The worst selling product(s) is/are : 
// "Charis Bash: 41";


// The bestseller product(s) is/are: 
// "Ergo LE22MT4W: 27"; 

// The worst selling product(s) is/are : 
// "Prology iScan-1100: 6, 
// FUGOO Tough: 6, 
// DNS PS-100: 6, 
// Street Storm STR-9040EX GL: 6";


// The bestseller type of product(s) is/are: 
// "Detector: 414"; 

// The worst selling type of product(s) is/are : 
// "Acoustic: 370";