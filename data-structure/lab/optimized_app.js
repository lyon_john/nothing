function display(arr, n) {
	var result = [];
	var new_arr = [];

	var user_name = {};

	var count = 0;
	var best_u, worst_u;

	var finalResult;

	// block 1.0*******************
	arr.forEach( function(element) {
		// transforming every element of the array into an array;
		new_arr = element.split(', ');

		// pushing every array into a new array;
		result.push(new_arr); 

	});

	// block 2.0*******************
	for(var i = 0; i < result.length; i++) {

		// counts how many purcheses every user made
		if (user_name[result[i][n - 1]] != null) {
			user_name[result[i][n - 1]] += 1;
		} else {
			user_name[result[i][n - 1]] = 1;
		}

	}

	// block 3.1*******************
	// counts which user made the most purcheses
	for (var u_key in user_name) {

		if(user_name[u_key] > count) {
			count = user_name[u_key];
			best_u = u_key + ': ' + user_name[u_key];

		} else if (user_name[u_key] == count) {
			best_u += ', \n' + u_key + ': ' + user_name[u_key];
			// checks if there are more then one leaders
		}

	}

	// block 3.2*********
	// counts which user made least purchases
	for (u_key in user_name) {

		if (user_name[u_key] < count) {
			count = user_name[u_key];
			worst_u = u_key + ': ' + user_name[u_key];

		} else if (user_name[u_key] == count) {
			worst_u += ', \n' + u_key + ': ' + user_name[u_key];
			// checks if there are more then one draw backs
		}
		
	}

	// storing in a nice looking string
	var user_sort = 'The best: \n"' + best_u + 
		'"; \n\nThe worst: \n"' + worst_u + '";';

	return user_sort;
}

console.log(display(data, prompt('which column do you want to compare(1-6)?')));