На входе случайная строка из слов, разделенных пробелами. Найти и вернуть:

a) Самое длинное слово;

```
search('I am gogoing to the beach')
> 'gogoing'
```

b) Самое короткое слово.

```
search('I am going to the beach')
> 'I'
```
