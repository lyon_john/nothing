angular.module('pagination', []);

angular
	.module('pagination')
	.filter('StartFrom', [StartFrom]);

function StartFrom () {
	return function (input, start) {
		start = +start;
		return input.slice(start);
	};
}

angular
	.module('pagination')
	.controller('PaginationController', [PaginationController]);

function PaginationController () {
	var self = this;

	self.currentPage = 0;
	self.itemsOnPage = 5;

	self.collection = [
		{
			name: 'item',
			id: ' #1',
			type: 'console'

		},{
			name: 'item',
			id: ' #2',
			type: 'PC'

		},{
			name: 'item',
			id: ' #3',
			type: 'car'

		},{
			name: 'item',
			id: ' #4',
			type: 'laptop'

		},{
			name: 'item',
			id: ' #5',
			type: 'console'

		},{
			name: 'item',
			id: ' #6',
			type: 'car'

		},{
			name: 'item',
			id: ' #7',
			type: 'laptop'

		},{
			name: 'item',
			id: ' #8',
			type: 'PC'

		},{
			name: 'item',
			id: ' #9',
			type: 'phone'

		},{
			name: 'item',
			id: ' #10',
			type: 'console'

		},{
			name: 'item',
			id: ' #11',
			type: 'PC'

		},{
			name: 'item',
			id: ' #12',
			type: 'phone'

		},{
			name: 'item',
			id: ' #13',
			type: 'console'

		},{
			name: 'item',
			id: ' #14',
			type: 'phone'

		},{
			name: 'item',
			id: ' #15',
			type: 'PC'

		},{
			name: 'item',
			id: ' #16',
			type: 'laptop'

		}
	];

	// check if we are on the first page
	self.firstPage = function firstPage () {
		return self.currentPage == 0;
	};

	// check if we are on the last page
	self.lastPage = function lastPage () {
		var lastPageNum = Math.ceil(self.collection.length / self.itemsOnPage - 1);
		
		return self.currentPage == lastPageNum;
	};

	// count gow many pages are in the document
	self.numberOfPages = function numberOfPages () {
		return Math.ceil(self.collection.length / self.itemsOnPage);
	};

	// starting point to display from the array
	self.startingItem = function startingItem () {
		return self.currentPage * self.itemsOnPage;
	};

	// move to the next page
	self.next = function next () {
		self.currentPage += 1;
	};

	// move to the previous page
	self.back = function back () {
		self.currentPage -= 1;
	};
}