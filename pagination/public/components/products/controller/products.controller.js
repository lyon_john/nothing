(function () {
	'use strict';

	angular
		.module('products')
		.controller('ProductsController', ['$state', '$http', ProductsController]);

	function ProductsController ($state, $http) {
		var self = this;

		self.products = [];

		self.getProduct = function getProduct (id) {
			$state.go('main.product.details', {id: id});
		};

		self.getAllProducts = function getAllProducts () {
			$http.get('/products')
				.then(function success (response) {
					self.products = response.data;
					//console.log(response);
				}, function error(error) {
					console.log(error);
				});
		};

		self.getAllProducts();
	}
}());