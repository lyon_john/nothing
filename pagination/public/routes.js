(function () {
	'use strict';

	angular
		.module('products')
		.config(routes);

	function routes ($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state({
				name: 'main',
				url: '/main',
				controller: 'MainController',
				controllerAs: 'main',
				templateUrl: 'components/main/template/main.template.html'
			})
			.state({
				name: 'main.products',
				url: '/products',
				controller: 'ProductsController',
				controllerAs: 'products',
				templateUrl: 'components/products/template/products.template.html'
			})
			.state({
				name: 'main.product',
				url: '/product',
				controller: 'ProductController',
				controllerAs: 'product',
				templateUrl: 'components/products/template/product.template.html'
			})
			.state({
				name: 'main.product.details',
				url: '/details/:id',
				templateUrl: 'components/products/template/product-details.template.html'
			})
			.state({
				name: 'main.product.edit',
				url: '/edit/:id',
				templateUrl: 'components/products/template/form.template.html'
			})
			.state({
				name: 'main.product.add',
				url: '/add',
				templateUrl: 'components/products/template/form.template.html'
			});

		$urlRouterProvider.otherwise('main');
	}
} ());