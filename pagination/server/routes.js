exports.routes = function routes(app){
	var products = require('./components/products.js');

	

	app.get('/products', products.getAllProducts);
	app.get('/products/:id', products.getProductById);
	app.post('/products', products.addProduct);
	app.delete('/products/:id', products.delProduct);
	app.put('/products/:id', products.editProduct);

};

