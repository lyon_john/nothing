var productList = [];

var Uuid = require('node-uuid');

exports.getAllProducts = function getAllProducts(req, res) {
	//control of product list
	if (productList.length === 0) {
		return res.status(404).send('Sorry, there are no products in stock.');
	}
	res.send(productList);
};

exports.getProductById = function getProductById(req, res) {
	if (!req.params.id) {
		return res.status(409).send('ID is mandatory');
	}
	productList.forEach(function(item) {
		if (item.id === req.params.id) {
			return res.status(200).send(item);
		}
	});

	res.status(404).send('Not found such ID!');
};

exports.addProduct = function addProduct(req, res) {
	//controll of blank property
	if (!req.body.name) {
		return res.status(409).send('Name of product is mandatory');
	}
	if (!req.body.model) {
		return res.status(409).send('Model is mandatory');
	}
	if (!req.body.description) {
		return res.status(409).send('Description is mandatory');
	}
	if (!req.body.price) {
		return res.status(409).send('Price is mandatory');
	}

	//creating an object wich will be pushed in general array
	var entryData = req.body;
	entryData.id = 'ID_' + Uuid.v4(); // creting new property - id and
	//generating ID
	productList.push(entryData); //pushing new object to general array
	res.send(req.body.name + ' was added succesufully!'); // respons from server
};

exports.delProduct = function delProduct(req, res) {
	//controll of blank property
	if (!req.params.id) {
		return res.status(409).send('ID is mandatory');
	}
	productList.forEach(function(item, i) {
		if (item.id === req.params.id) {
			productList.splice(i, 1);
			return res.status(200).send('Product with id: ' + req.params.id +
				' was succesufully deleted');
		}
	});
	res.status(404).send('Not found such id');
};
exports.editProduct = function editProduct(req, res) {

	if (!req.params.id) {
		return res.status(409).send('ID is mandatory');
	}

	var userIdExists;
	for (var i = 0; i < productList.length; i++) {
		if (productList[i].id === req.params.id) {
			productList[i].name = req.body.name;
			productList[i].model = req.body.model;
			productList[i].description = req.body.description;
			productList[i].price = req.body.price;
			userIdExists = true;
			return res.status(200).send('Product with id: ' + req.params.id +
				' was succesufully updated!');
		}
	}
	if (!userIdExists) {
		res.status(404).send('Not found such id');
	}
};