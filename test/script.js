'use strict';
var list1 = {
	id: 1,
	name: 'john',
	text: 'Hi'
};

var list2 = {
	id: 2,
	name: 'boris',
	text: 'Mizerie',
	lastname: 'Hincu'
};

var list3 = {
	id: 3,
	name: 'hamster',
	text: '4ic 4ic',
};

var list4 = {
	id: 1
};


function Store() {

	var result = [];

	this.add = function(obj) {
		result.push(obj);
	};
	// end of add
	this.fetchAll = function() {
		return result;
	};
	// end of fetch all
	this.fetch = function(objVal) {
		var count = 0;
		var arr = [];
		var noRepeat = 0;
		var keys = Object.keys(objVal);

		result.forEach(function(element) {

			for (var i = 0; i < keys.length; i++) {
				if (objVal[keys[i]] == element[keys[i]]) {
					count ++;
				}
			}

			if (count == keys.length) {
				// if there are more then one obj to grab
				// fetch the first one encountered
				if (noRepeat == 0) {
					arr.push(element);
					noRepeat++;
				}
			}

			count = 0;
		});
		
		return arr;
	};
	// end of fetch
	this.update = function(obj, newObj) {
		var newKeys = Object.keys(newObj);
		
		result.forEach(function(element) {
			if (element == obj) {
				element[newKeys] = newObj[newKeys];
			}
		});

	};
	// end of update
	this.delete = function(obj) {

		result.forEach(function(element, index) {
			if (element == obj) {
				result.splice(index, 1);
			}
		});

	};
	// end of delete
}

var store = new Store();

store.add(list1);
store.add(list1);
store.add(list2);
store.add(list3);
store.add(list4);
console.log(store.fetchAll());
console.log(store.fetch({id:1}));
store.update(list1, {isPussy: 'yes'}) ;
store.delete(list1);
store.delete(list1);
console.log(store.fetchAll());
