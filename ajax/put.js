'use strict';

var doc = document.getElementById('put');

doc.onclick = function() {

	var newObj = {
		EntryId: document.getElementById('entryId').value,
		Author: document.getElementById('Author').value,
		Title: document.getElementById('Title').value,
		Text: document.getElementById('Text').value
	};

	var put = function(obj, cb) {
		var xhttp = new XMLHttpRequest();
		var url = 'http://localhost:3333/entry';

		xhttp.open('PUT', url);
		xhttp.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');

		xhttp.addEventListener('load', function() {
			cb(this.responseText);
		});

		xhttp.addEventListener('error', function() {
			cb(this.status + ': ' + this.statusText);
		});

		xhttp.send(JSON.stringify(obj));
	};



	put(newObj, function(a) {
		document.getElementById('demo').innerHTML = a;
	});
	
	document.getElementById('entryId').value = '';
	document.getElementById('Author').value = '';
	document.getElementById('Title').value = '';
	document.getElementById('Text').value = '';
};
