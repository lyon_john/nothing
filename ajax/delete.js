'use strict';

var doc = document.getElementById('delete');

doc.onclick = function() {

	var objID = {
		EntryId: document.getElementById('entryId').value
	};

	var deleteFunc = function(obj, cb) {

		var xhttp = new XMLHttpRequest();
		var url = 'http://localhost:3333/entry';

		xhttp.open('DELETE', url);
		xhttp.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');

		xhttp.addEventListener('load', function() {
			cb(this.responseText);
		});

		xhttp.addEventListener('error', function() {
			cb(this.status + ': ' + this.statusText);
			console.log(this.status + ': ' + this.statusText);
		});

		xhttp.send(JSON.stringify(obj));

	};

	deleteFunc(objID, function(e) {
		document.getElementById('demo').innerHTML = e;
	});

	document.getElementById('entryId').value = '';
};