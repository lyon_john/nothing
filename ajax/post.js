'use strict';

var doc = document.getElementById('post');

doc.onclick = function() {
	
	var boss = {
		Author: document.getElementById('Author').value,
		Title: document.getElementById('Title').value,
		Text: document.getElementById('Text').value
	};

	var post = function(obj, cb) {
		var xhttp = new XMLHttpRequest();
		var url = 'http://localhost:3333/entry';

		xhttp.open('POST', url);
		xhttp.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
		xhttp.addEventListener('load', loadingF);
		xhttp.addEventListener('error', errorF);

		var loadingF = function() {
			cb(this.responseText);
		};

		var errorF = function() {
			cb(this.status + ': ' + this.statusText);
		};

		xhttp.send(JSON.stringify(obj));
	};


	document.getElementById('demo').innerHTML = 'post succes';

	post(boss, function(a) {
		console.log(a);
	});

	document.getElementById('Author').value = '';
	document.getElementById('Title').value = '';
	document.getElementById('Text').value = '';
};
