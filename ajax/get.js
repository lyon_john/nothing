'use strict';

var doc = document.getElementById('get');

doc.onclick = function() {
	
	var get = function(cb) {
		var xhttp = new XMLHttpRequest();
		var url = 'http://localhost:3333/entry';

		xhttp.open('GET', url);

		xhttp.addEventListener('load', function() {
			cb(xhttp.responseText);
		});

		xhttp.addEventListener('error', function() {
			console.log(xhttp.status + ': ' + xhttp.statusText);
		});

		xhttp.send();
	};



	get(function(a) {
		document.getElementById('demo').innerHTML = a;
	});
	
};