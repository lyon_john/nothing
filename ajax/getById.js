'use strict';

var doc = document.getElementById('getById');

doc.onclick = function() {

	var boss = {
		EntryId: document.getElementById('entryId').value
	};

	var get = function(obj, cb) {

		var xhttp = new XMLHttpRequest();
		var url = 'http://localhost:3333/entry/' + document.getElementById('entryId').value;

		xhttp.open('GET', url);

		xhttp.addEventListener('load', function() {
			cb(xhttp.responseText);
		});

		xhttp.addEventListener('error', function() {
			console.log(xhttp.status + ': ' + xhttp.statusText);
		});

		xhttp.send();

	};

	get(boss, function(a) {
		document.getElementById('demo').innerHTML = a;
	});

	document.getElementById('entryId').value = '';
};